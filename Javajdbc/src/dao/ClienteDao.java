
package dao;

import com.sun.istack.internal.logging.Logger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import model.Cliente;

public class ClienteDao {
    private DataSource dataSource;
    private Statement st;
    private Connection connection;
    public ClienteDao(DataSource dataSource){
        this.dataSource = dataSource;
    }
    public ArrayList<Cliente>readAll(){
        try{
            String SQL ="SELECT *from clientes";
            PreparedStatement ps=dataSource.getConnection().prepareStatement(SQL);
            ResultSet rs=ps.executeQuery();
            
            ArrayList<Cliente> lista = new ArrayList<Cliente>();
            while(rs.next()){
                Cliente cli= new Cliente();
                cli.setId(rs.getInt("id"));
                cli.setNome(rs.getString("nome"));
                cli.setEmail(rs.getString("email"));
                cli.setTelefone(rs.getString("telefone"));
                lista.add(cli);
            }
            ps.close();
            return lista;
        }catch(SQLException ex){
            System.err.println("Erro ao recuperar "+ex.getMessage());
        }
        catch(Exception ex){
            System.err.println("Erro geral "+ex.getMessage());
        }
       return null; 
    }
 
    public void inserir(String nome,String email,String telefone){
       try{
           String comando="INSERT INTO  clientes(nome,email,telefone)VALUES('"+nome+"','"+email+"','"+telefone+"')";
           st=connection.createStatement();
           st.executeUpdate(comando);
           st.close();
       }catch(SQLException e){
           System.out.println("Erro ao cadastrar");
       }
   }
    public void remover(int num){
       
       try{
           String comando="Delete from clientes where num="+num;
           st=connection.createStatement();
           st.executeUpdate(comando);
       }catch(SQLException e){
           System.out.println("Erro ao remover!!!\n"+e);  
       }
   }
    
}
